Commerce Stocked Default
========================
This module has no UI, enable it to ensure that the default product shown to
users on the add to cart form is an in-stock item.
